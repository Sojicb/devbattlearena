﻿using BattleArenaFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArenaFinal.GameModes
{
    public class OnevsOne
    {
        DataBaseAccess dataBaseAccess = new DataBaseAccess();

        public IHero ChooseHero()
        {
            Console.WriteLine("Press 1 to Choose Warrior");
            Console.WriteLine("Press 2 to Choose Paladin");
            switch (Convert.ToInt32(Console.ReadLine()))
            {
                case 1:
                    Warrior warrior = new Warrior(dataBaseAccess.HeroData.Get(1));
                    return warrior;
                case 2:
                    Paladin paladin = new Paladin(dataBaseAccess.HeroData.Get(2));
                    return paladin;
            }
            return null;
        }
    }
}
