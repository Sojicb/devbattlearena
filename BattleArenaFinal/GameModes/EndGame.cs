﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArenaFinal.GameModes
{
    public static class EndGame
    {
        public static void ExitGame(int PlayerOne, int PlayerTwo)
        {
            if (PlayerOne < 0)
            {
                Console.WriteLine("Warrior Wins");
            }
            else if (PlayerTwo < 0)
            {
                Console.WriteLine("Paladin Wins");
            }
        }
    }
}
