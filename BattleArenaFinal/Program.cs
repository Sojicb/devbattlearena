﻿using BattleArenaFinal.DataAccess;
using BattleArenaFinal.GameModes;
using BattleArenaFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArenaFinal
{
    public class Program
    {
        static void Main(string[] args)
        {
            OnevsOne mode = new OnevsOne();
            DataBaseAccess dataBaseAccess = new DataBaseAccess();
            IHero playerOne = mode.ChooseHero();
            IHero playerTwo = mode.ChooseHero();
            int damage;
            while (!(playerOne.CurrentHealth <= 0 || playerTwo.CurrentHealth <= 0))
            {
                damage = playerTwo.CastSpell();
                playerOne.CurrentHealth += damage;
                if(playerOne.CurrentHealth < 0 || playerTwo.CurrentHealth < 0)
                {
                    break;
                }
                damage = playerOne.CastSpell();
                playerTwo.CurrentHealth += damage;
            }
            EndGame.ExitGame(playerTwo.CurrentHealth, playerOne.CurrentHealth);
        }
    }
}
