﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArenaFinal.Models
{
    public interface IHero
    {
        int Id { get; set; }
        string Name { get; set; }
        int MaxHealth { get; set; }
        int CurrentHealth { get; set; }
        int MaxMana { get; set; }
        int CurrentMana { get; set; }
        int MaxDamage { get; set; }
        int MinDamage { get; set; }
        int CastSpell();
    }
}
