﻿using BattleArenaFinal.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArenaFinal.Models
{
    public class Warrior : IHero
    {
        Random random = new Random();

        public int Id { get; set; }
        public string Name { get; set; }
        public int MaxHealth { get; set; }
        public int CurrentHealth { get; set; }
        public int MaxMana { get; set; }
        public int CurrentMana { get; set; }
        public int MaxDamage { get; set; }
        public int MinDamage { get; set; }

        public Warrior(HeroDB heroModel)
        {
            Id = heroModel.Id;
            Name = heroModel.Name;
            MaxHealth = heroModel.MaxHealth;
            MaxMana = heroModel.MaxMana;
            MaxDamage = heroModel.MaxDamage;
            MinDamage = heroModel.MinDamage;
            CurrentHealth = heroModel.MaxHealth;
            CurrentMana = heroModel.MaxMana;
        }

        public int BasicAttack()
        {
            return -random.Next(MinDamage, MaxDamage);
        }

        public int HeroicStrike()
        {
            return -200;
        }

        public int CastSpell()
        {
            int key;
            Console.WriteLine("===================================");
            Console.WriteLine("Press 1 for Basic Attack");
            Console.WriteLine("Press 2 for Heroic Strike");
            Console.WriteLine("===================================");
            Console.WriteLine(Name);
            Console.WriteLine(CurrentHealth);
            Console.WriteLine(CurrentMana);
            Console.WriteLine("===================================");
            key = Convert.ToInt32(Console.ReadLine());
            switch (key)
            {
                case 1:
                    Console.WriteLine(Name + " Casts Basic Attack");
                    return BasicAttack();
                case 2:
                    Console.WriteLine(Name + " Casts Heroic Strike");
                    return HeroicStrike();
            }
            return 0;
        }
    }
}
