﻿using BattleArenaFinal.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleArenaFinal.DataAccess
{
    public class HeroData
    {
        private readonly string _connectionString;
        public HeroData(string connectionString)
        {
            _connectionString = connectionString;
        }

        public int Add(HeroDB model)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "INSERT INTO Hero(Name, MaxHealth, MaxMana, MaxDamage, MinDamage) OUTPUT Inserted.Id " +
                        "VALUES(@Name, @MaxHealth, @MaxMana, @MaxDamage, @MinDamage)";
                    sqlCommand.Parameters.AddWithValue("@Name", model.Name);
                    sqlCommand.Parameters.AddWithValue("@MaxHealth", model.MaxHealth);
                    sqlCommand.Parameters.AddWithValue("@MaxMana", model.MaxMana);
                    sqlCommand.Parameters.AddWithValue("@MinDamage", model.MinDamage);
                    sqlCommand.Parameters.AddWithValue("@MaxDamage", model.MaxDamage);

                    return (int)sqlCommand.ExecuteScalar();
                }
            }
        }

        public string Delete(int id)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "DELETE FROM Hero OUTPUT deleted.Name WHERE Id=@Id";
                    sqlCommand.Parameters.AddWithValue("@Id", id);
                    sqlCommand.ExecuteNonQuery();

                    return sqlCommand.ExecuteScalar() as string;
                }
            }
        }

        public int Update(HeroDB model)
        {
            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "UPDATE Hero SET Name=@Name, MaxHealth=@Health, MaxMana=@Mana, MaxDamage=@MaxDamage, " +
                        "MinDamage=@MinDamage OUTPUT Inserted.Id where Id=@Id";
                    sqlCommand.Parameters.AddWithValue("@Name", model.Name);
                    sqlCommand.Parameters.AddWithValue("@Health", model.MaxHealth);
                    sqlCommand.Parameters.AddWithValue("@Mana", model.MaxMana);
                    sqlCommand.Parameters.AddWithValue("@MinDamage", model.MinDamage);
                    sqlCommand.Parameters.AddWithValue("@MaxDamage", model.MaxDamage);
                    sqlCommand.Parameters.AddWithValue("@Id", model.Id);
                    sqlCommand.ExecuteNonQuery();

                    return (int)sqlCommand.ExecuteScalar();
                }
            }
        }

        public HeroDB Get(int id)
        {
            HeroDB model = new HeroDB();

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
                {
                    sqlCommand.CommandText = "SELECT * FROM Hero WHERE Id = @Id";
                    sqlCommand.Parameters.AddWithValue("@Id", id);

                    using (SqlDataReader reader = sqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            model.Id = (int)reader["Id"];
                            model.Name = reader["Name"] as string;
                            model.MaxHealth = (int)reader["MaxHealth"];
                            model.MaxMana = (int)reader["MaxMana"];
                            model.MinDamage = (int)reader["MinDamage"];
                            model.MaxDamage = (int)reader["MaxDamage"];
                        }
                    }
                }
            }
            return model;
        }
    }
}

